
import { useCart } from '../components/CartContext';
import './ShoppingCart.css';

// Shipping cart component that displays the Products list added to the cart

const ShoppingCart = () => {
    const { cart, removeFromCart, clearCart } = useCart();

    return (
        <div className="cart-container">
            <h2>Your Cart</h2>
            {cart.length === 0 ? (
                <p>Your cart is empty.</p>
            ) : (
                <>
                    <ul className="cart-items">
                        {cart.map((item) => (
                            <li key={item.id} className="cart-item">
                                <img src={item.image} alt={item.title} />
                                <div>
                                    <h3>{item.title}</h3>
                                    <p>${item.price} x {item.quantity}</p>
                                    <button onClick={() => removeFromCart(item.id)}>Remove</button>
                                </div>
                            </li>
                        ))}
                    </ul>
                    <button onClick={clearCart} className="clear-cart">Clear Cart</button>
                </>
            )}
        </div>
    );
};

export default ShoppingCart;