import Jacket from "../assets/Jacket.jpg";
import croptop from "../assets/croptop.jpg";
import sleeves from "../assets/sleeves.jpg";

import './Home.css';



// Home Component that will return the current landpage of the website

const Home = () => {
    return (
        <div className="home-container">
            <header className="hero-section">
                <h1>Welcome to Our Shop</h1>
                <p>Your one-stop destination for all your needs.</p>
                <marquee>Welcome Offer: New customers get 10% off their first purchase.</marquee>
            </header>
            <section className="featured-products">
                <h2>New Arrivals</h2>
                <div className="product-list">
                    <div className="product-item">
                        <img src={sleeves} alt="Product 1" />
                        <h3>OutCast</h3>
                        <p>$20.00</p>
                    </div>
                    <div className="product-item">
                        <img src={Jacket} alt="Product 2" />
                        <h3>Puffer Jacket</h3>
                        <p>$100.00</p>
                    </div>
                    <div className="product-item">
                        <img src={croptop} alt="Product 3" />
                        <h3>Crop top</h3>
                        <p>$10.00</p>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default Home;