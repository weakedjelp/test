import  { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

// CartContext component is responsible for storing the information about the product added to the cart

const CartContext = createContext();
export const useCart = () => useContext(CartContext);
export const CartProvider = ({ children }) => {

    CartProvider.propTypes = {
        children: PropTypes.node.isRequired,
    };
    const [cart, setCart] = useState([]);

    const addToCart = (product) => {
        console.log('Adding product to cart:', product);
        setCart((prevCart) => {
            const productExists = prevCart.find(item => item.id === product.id);
            if (productExists) {
                return prevCart.map(item =>
                    item.id === product.id ? { ...item, quantity: item.quantity + 1 } : item
                );
            }
            return [...prevCart, { ...product, quantity: 1 }];
        });
    };

    const removeFromCart = (productId) => {
        setCart((prevCart) => prevCart.filter(item => item.id !== productId));
    };

    const clearCart = () => setCart([]);

    return (
        <CartContext.Provider value={{ cart, addToCart, removeFromCart, clearCart }}>
            {children}
        </CartContext.Provider>
    );
};