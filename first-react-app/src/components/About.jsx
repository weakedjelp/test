import './About.css';

// About Component that will return the companies information
const About = () => {
    return (
        <div className="about-container">
            <h2>About Us</h2>
            <p>Welcome to our company. We are committed to providing the best products and services to our customers. Our mission is to deliver high-quality products that meet our customers needs and exceed their expectations.</p>
            <p>Our team is made up of dedicated professionals who are passionate about their work. We believe in innovation, integrity, and excellence in everything we do.</p>
            <p>Thank you for choosing our company. We look forward to serving you and meeting your needs.</p>
        </div>
    );
};

export default About;