
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Navbar from "./Navbar";
import Home from './components/Home';
import Products from './components/Products';
import About from './components/About';
import Contact from './components/Contact';
import ShoppingCart from './components/ShoppingCart';
import { CartProvider } from './components/CartContext';
import './App.css';

// App Component that will be used to display all the components that are needed to display
        
const App = () => {
    return (
        <CartProvider>
      <Router>
            <Navbar />
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/products" element={<Products />} />
                <Route path="/about" element={<About />} />
                <Route path="/contact" element={<Contact />} />
                <Route path="/ShoppingCart" element={<ShoppingCart/>} />
                <Route path="*" element="Page not Found"/>
            </Routes>
             </Router>

        </CartProvider>


    );
};

export default App;